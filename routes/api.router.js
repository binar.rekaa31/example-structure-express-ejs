const express = require('express')
const { getAllCars, newCars, showCar, editCars, deleteCar } = require('../controllers/api/cars.controller')
const { getAllSize } = require('../controllers/api/size.controller')
const router = express.Router()

// Size Route API
router.get('/size', getAllSize)

// Cars Route API
router.get('/cars', getAllCars)
router.post('/cars', newCars)
router.get('/cars/:id', showCar)
router.post('/cars/:id', editCars)
router.delete('/cars/:id', deleteCar)

module.exports = router