const express = require('express')
const { create } = require('../controllers/web/create.controller')
const { edit } = require('../controllers/web/edit.controller')
const { index } = require('../controllers/web/index.controller')
const router = express.Router()

router.get('/', index)
router.get('/create', create)
router.get('/edit/:id', edit)

module.exports = router